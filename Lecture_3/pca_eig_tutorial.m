% Metoda PCA, dle https://www.cs.cmu.edu/~elaw/papers/pca.pdf
% Shlens, A Tutorial on Principal Component Analysis, 2005

data=[2 1 0; 4 3 0]
[M,N]=size(data)            % M je pocet senzoru, N je pocet pozorovani (cas)

mn=0*mean(data,2)           % spravne bychom meli odecist stredni hodnotu
data=data-repmat(mn,1,N)    

covariance=(1/(N-1))*data*data' % vypocet kovariancni matice. Povsimnete si ze neni diagonalni

[PC,V]=eig(covariance)  % Eigendekompozice. Columns of PC are eigenvectors (help)

V=diag(V)               % matice V obsahuje eigenvalues hodnoty kovariancni matice, 
                        % ale narozdil od SVD nejsou setridene
[junk,rindices]=sort(-1*V)      % setridime od nejvetsiho (jsou normalne kladna)
V=V(rindices)                   % a preskladame  matici eigenvalues, aby sla od nejvetsiho
PC=PC(:,rindices)               % matici principal component seradime tak, aby
                                % nejdulezitejsi novy bazovy vektor byl
                                % prvni sloupec
P=PC'
signals=P*data                % nase matice P dle tutorialu ma obsahovat bazovevektory v radcich, proto transponujeme
                                % a delame projekci Y=PX
                                
signals*signals'                 % overime ze matice bodu po projekci  je diagonalni

reconstructed_perfect=P'*signals        % po rekonstrukci dostaneme puvodni body
error_perfect=reconstructed_perfect-data

reconstructed_reduced=P'*[signals(1,:); 0*signals(2,:)] % redukce dimenze - vynulujeme rozmer s malym vykonem
error_reduced=reconstructed_reduced-data

