function [P,T] = reorder( A, B)

% random reordering of training couples

[rA, c] = size( A);       % number of rows and columns of matrix A
[rB, c] = size( B);       % number of rows and columns of matrix B

% cA == cB : number of training patterns

P = zeros( rA, c);        % re-ordered matrix of input patterns
T = zeros( rB, c);        % re-ordered matrix of output responses

ind = 1+floor( abs( rand*c-0.001));          % random column (train.pattern)

for n=1:c                 % OVER ALL TRAINING PATTERNS
  while A( :,ind)==-1                  % if column already proceeded
    ind = 1+floor( abs( rand*c-0.001));      % go to another column
  end
  P( :,n)  = A( :,ind);   % unproceeded column to n-th position
  T( :,n)  = B( :,ind);
  A( :,ind)= -1;          % column already proceeded
end