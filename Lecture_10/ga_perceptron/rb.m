function F = rb( x1, x2)

F = 100*( x2 - x1^2)^2 + (1-x1)^2;

end

