function out = nonlin( w, b, x, fct)

if fct==1
    out = tansig( [w, b]*x);
elseif fct==2
    out = logsig( [w, b]*x);
end