function out = decim( gen, pc, pm, I, geb)

% breeding new generation using population decimation

% gen  matrix of chromosomes
% pc   probability of crossover
% pm   probability of mutation
% I    number of individuals in generation
% geb  number of bits in chromosome

coup = round( I/2);                     % number of couples of parents
doup = coup - 1;

          % ORDERING INDIVIDUALS ACCORDING TO QUALITY

par = zeros( coup, geb);
for n=1:coup
  [val,ind] = min( gen( :,geb));        % individual with minimal cost
  par( n,:) = gen( ind, :);
  gen( ind, geb) = 1e+20;
end

          % CROSSOVER, MUTATION

chld = zeros( I, geb);
ind = 1;

chld( 1,:) = par( 1,:);                 % elitism
ind = ind + 1;

for n=1:coup
  ind0 = 1 + round( rand*doup);              % index of 1st parent
  ind1 = 1 + round( rand*doup);              % index of 2nd parent
  cros = round( rand*geb/pc);                % crossover
  icrs = cros + 1;
  if cros<geb
    chld( ind+0, 1   :cros) = par( ind0, 1   :cros);
    chld( ind+0, icrs:geb)  = par( ind1, icrs:geb);
    chld( ind+1, 1   :cros) = par( ind1, 1   :cros);
    chld( ind+1, icrs:geb)  = par( ind0, icrs:geb);
  else
    chld( ind+0,1:geb) = par( ind0,1:geb);	 % no crossover
    chld( ind+1,1:geb) = par( ind1,1:geb);
  end;

  mut = ceil( rand*geb/pm);                  % mutation
  if mut<geb
    chld( ind+0, mut) = 1 - chld( ind+0, mut);
  end
  mut = ceil( rand*geb/pm);
  if mut<geb
    chld( ind+1, mut) = 1 - chld( ind+1, mut);
  end
    
  ind = ind + 2;
end

out = chld(1:I,:);