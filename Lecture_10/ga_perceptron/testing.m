function testing( w, b, nlin, Tmax)

ind = 1;
for m=1:7
    x1 = m*0.5 - 1.8;
    for n=1:7
        x2 = n*0.5 - 0.7;
        P(1,ind) = x1;
        P(2,ind) = x2;
        T1(m,n) = rb( x1, x2);
        T2(m,n) = Tmax*nsim( P(:,ind), w, b, nlin);
        ind = ind + 1;
    end
end

figure; mesh(T1);
figure; mesh(T2);