function out = nsim( P, w, b, fct)

% computing response of the network to the input pattern P

global  N

% N  number of neurons in layers
% P  an input pattern
% w  vector of synaptic weights
% b  vector of biases

% fct == 1  symmetric sigmoid
% fct == 2  asymmetric sigmoid

nlay = size( N, 2);           % number of layers

lw  = 1;                      % index of 1st weight of 1st neuron
up  = N(1);                   % index of last weight of last neuron
ind = 1;                      % index of actual neuron

x = [P;  -1];                 % output of input layer (-1 for bias)

for m = 2:nlay                % OVER ALL HIDDEN LAYERS
  
  y   = zeros( N( m), 1);           % clearing outputs of a layer
  
  for n = 1:N(m)                    % OVER ALL NEURONS IN A LAYER
    y(n) = nonlin( w(lw:up), b( ind), x, fct( m-1));
    lw  = up + 1;
    up  = up + N( m-1);                  % updating indexes
    ind = ind + 1;
  end
  
  up = up - N( m-1) + N( m);  % go to the next layer
  x = [y; -1];                % output of actual layer = input of next one
  
end

out = y;                      % output of last layer = output of network