function [w,b] = decode( gen)

% binary genes transformed to real synaptic weights and biases

global  ind  wbit  nwgh  nbia

% ind   number of individuals in generation
% wbit  number of bits per synaptic weight and biases
% nwgh  number of synaptic weights
% nbia  number of biases
% gen   matrix of chromosomes

dwbit = wbit - 1;

for p=1:ind        % OVER ALL INDIVIDUALS
  
  lw = 1;               % index of lowest and highest bit of weight
  up = wbit;            % in chromosome
  
                        % OVER ALL WEIGHTS
  for q=1:nwgh
    w( p,q) = gen( p,lw:up) * pow2(0:dwbit)' / pow2(dwbit) - 1;
    lw = lw + wbit; up = up + wbit;
  end

                        % OVER ALL BIASES
  for q=1:nbia
    b( p,q) = gen( p,lw:up) * pow2(0:dwbit)' / pow2(dwbit) - 1;
    lw = lw + wbit; up = up + wbit;
  end
  
end