function [wb, bb, nlin, Tmax] = trainga()

global  N  ind  wbit  nwgh  nbia

N = [2  5  5  1];             % number of neurons in layers

ind = 1;
for m=1:8
    x1 = m*0.5 - 2;
    for n=1:8
        x2 = n*0.5 - 1;
        P(1,ind) = x1;
        P(2,ind) = x2;
        T(1,ind) = rb( x1, x2);
        ind = ind + 1;
    end
end
     
Tmax = max( max( T));         % norming output targets
T    = T / Tmax;

ind  =   40;                  % number of individuals in a generation
iter =   50;                  % number of learning iterations (generations)
wbit =    8;                  % number of bits per synaptic weight and bias
pcr  = 0.90;                  % probability of crossover
pmut = 0.10;                  % probability of mutation

nlin = [ 1  1  2];            % 1=symmetric, 2=asymmetric sigmoid in a layer


% -------------------------------------------------


nlay = size( N, 2);           % number of layers

nwgh = 0;                     % number of synaptic weights
for m = 2:nlay
  nwgh = nwgh + N(m-1)*N(m);
end
nbia = norm( N,1) - N(1);     % number of biases

if mod( ind, 2) == 1          % if number of individuals odd...
  ind = ind + 1;              % then add one individual
end;

pat  = size( P, 2);           % number of training patterns
cbit = (nwgh+nbia)*wbit + 1;  % number of bits in chromosome + cost


gen = round( rand( ind, cbit));          % FIRST GENERATION


for i=1:iter              % OVER ALL GENERATIONS
  
  [P,T] = reorder( P,T);       % reordering training patterns
  [w,b] = decode( gen);        % binary genes -> real weights, biases
  
  for m=1:ind                  % OVER ALL INDIVIDUALS
    err( m, i) = 0;                       % error of m-th individual in i-th generation
    for n=1:pat                           % OVER ALL TRAINING PATTERNS
      out = nsim( P(:,n), w(m,:),b(m,:), nlin); % response of m-th net to n-th pattern
      dif = out - T( :,n);                      % diff. betw. actual response and desired one
      err( m, i) = err( m, i) + dif'*dif;       % error of m-th net in i-th iteration
    end                                   % END ALL TRAINING PATTERNS
    gen( m,cbit) = err( m,i);             % storing cost function
  end                          % END ALL INDIVIDUALS
  
  [val,idx] = min( gen( :,cbit));   % index of individual with minimal cost
  e(i) = val;                       % value of minimal cost in generation
  wb = w(idx,:);         % parameters of the best individual
  bb = b(idx,:);         % parameters of the best individual
  
                          % BREEDING NEW INDIVIDUALS
  
  gen = decim( gen, pcr, pmut, ind, cbit);
  
end                       % END ALL GENERATIONS

figure(6); plot( e);

% save  x  e  Tmax  nwgh  nbia  N  nlin