clear all
close all
N=2000;


CFO1=10*randn(1,N)-200;
GI1=0.1*randn(1,N)+1;
CFO2=1*randn(1,N)-200;
GI2=0.1*randn(1,N)-1;
CFO3=3*randn(1,N)-220;
GI3=0.1*randn(1,N)-0.5;

DataAll=[CFO1,CFO2,CFO3;GI1,GI2,GI3]';

for i=1:length(DataAll) %  computing the classes
    if(i<=length(DataAll)/3) 
        C(i)={'1'};
    elseif (i<=length(DataAll)*2/3) && (i>length(DataAll)/3)
        C(i)= {'2'};
    else
        C(i)= {'3'};
    end
end
C=C';

c = cvpartition(length(DataAll),'Holdout',10)  
idxTrain = training(c);
DataTrain = DataAll(idxTrain,:);
ClassTrain= C(idxTrain,:);
idxtest = test(c);
DataTest = DataAll(idxtest,:);
ClassTest= C(idxtest,:);

%SVM= fitcsvm(DataAll,C); % Train SVM classifier using the Data and the classes
%suportvectors = SVM.SupportVectors;% get the suppport vectors
template = templateSVM( 'Standardize', false,'SaveSupportVectors',true,'KernelFunction','Linear');
SVMMul= fitcecoc(DataTrain,ClassTrain,'Learners',template);
LearnerNo=1
figure(LearnerNo)
gscatter(DataTrain(:,1),DataTrain(:,2),ClassTrain,'brc','xxx',5); % plot the Data
hold on    
plot(SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,1),SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,2),'md','MarkerSize',10)
%plot(suportvectors(:,1),suportvectors(:,2),'o','MarkerSize',10) %Plot the support vectors
xline=linspace(min([CFO1 CFO2 CFO3]), max([CFO1 CFO2 CFO3]),100);
yline=(-SVMMul.BinaryLearners{LearnerNo}.Bias-SVMMul.BinaryLearners{LearnerNo}.Beta(1)*xline)/SVMMul.BinaryLearners{LearnerNo}.Beta(2)
plot(xline,yline,'g')
xlabel('Carrier Frequency Offset [Hz]');ylabel('Gain Imbalance [dB]')

LearnerNo=2
figure(LearnerNo)
gscatter(DataTrain(:,1),DataTrain(:,2),ClassTrain,'brc','xxx',5); % plot the Data
hold on    
plot(SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,1),SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,2),'md','MarkerSize',10)
%plot(suportvectors(:,1),suportvectors(:,2),'o','MarkerSize',10) %Plot the support vectors
xline=linspace(min([CFO1 CFO2 CFO3]), max([CFO1 CFO2 CFO3]),100);
yline=(-SVMMul.BinaryLearners{LearnerNo}.Bias-SVMMul.BinaryLearners{LearnerNo}.Beta(1)*xline)/SVMMul.BinaryLearners{LearnerNo}.Beta(2)
plot(xline,yline,'g')
xlabel('Carrier Frequency Offset [Hz]');ylabel('Gain Imbalance [dB]')

LearnerNo=3
figure(LearnerNo)
gscatter(DataTrain(:,1),DataTrain(:,2),ClassTrain,'brc','xxx',5); % plot the Data
hold on    
plot(SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,1),SVMMul.BinaryLearners{LearnerNo}.SupportVectors(:,2),'md','MarkerSize',10)
%plot(suportvectors(:,1),suportvectors(:,2),'o','MarkerSize',10) %Plot the support vectors
xline=linspace(min([CFO1 CFO2 CFO3]), max([CFO1 CFO2 CFO3]),100);
yline=(-SVMMul.BinaryLearners{LearnerNo}.Bias-SVMMul.BinaryLearners{LearnerNo}.Beta(1)*xline)/SVMMul.BinaryLearners{LearnerNo}.Beta(2)
plot(xline,yline,'g')
xlabel('Carrier Frequency Offset [Hz]');ylabel('Gain Imbalance [dB]')
ylim([-1.5 1.5])


predict( SVMMul.BinaryLearners{LearnerNo},DataTest)







